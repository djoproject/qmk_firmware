# Custom Sofle layout

[old readme](./readme.old.md)

## Setup

```
python -m pip install qmk
qmk_setup
```

## Compile

```
qmk compile -kb sofle -km jo
```

## Push to keyboard

```
qmk flash -kb sofle -km jo
```
