#include QMK_KEYBOARD_H

/*
 * https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
 *
 * TODO try to set microphone control on right encoder
 *  does not seem possible
 *  currently pgup/pgdown, not really usefull.
 *
 * TODO not sure macos/linux keys swap is going to be useful
 *      * it exchanges lctr with lgui
 *      * it exchanges rctr with rgui
 *      * TODO check it
 *      * TODO what is CMD ? lgui/rgui
 *          OPT1 need to change behaviour to exchange lgui/lalt and rgui/ralt
 *          OPT2 need to disable this behaviour
 *
 */

enum sofle_layers {
    _QWERTY,
    _RAISE,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * QWERTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ` ~  |  1 ! |  2 @ |  3 # |  4 $ |  5 % |                    |  6 ^ |  7 & |  8 * |  9 ( |  0 ( | Bspc |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  \ | |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Caps |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |  ; : |  ' " |
 * |------+------+------+------+------+------|  MUTE |    |MICMUTE|------+------+------+------+------+------|
 * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |  , < |  . > |  / ? |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *            | LCTR | LGUI | LAlt |RAISE | /Space  /       \Enter \  |RAISE | RAlt | RGUI | RCTR |
 *            |      |      |      |      |/       /         \      \ |      |      |      |      |
 *            `----------------------------------'           '------''---------------------------'
 */

[_QWERTY] = LAYOUT(
  KC_GRV,   KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                     KC_6,    KC_7,    KC_8,    KC_9,    KC_0,  KC_BSPC,
  KC_TAB,   KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,  KC_BSLS,
  KC_CAPS,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                     KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN,  KC_QUOT,
  KC_LSFT,  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_MUTE,      KC_F20,KC_N,    KC_M, KC_COMM,  KC_DOT, KC_SLSH,  KC_RSFT,
                 KC_LCTL,KC_LGUI,KC_LALT, MO(_RAISE), KC_SPC,       KC_ENT, MO(_RAISE), KC_RALT, KC_RGUI, KC_RCTL
),

/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |  F1  |  F2  |  F3  |  F4  |  F5  |                    |  F6  |  F7  |  F8  | _ -  | + =  | Del  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  | ESC  |  Up  |      |      | Pscr |                    | Ins  | Home | PgUp | { [  | } ]  | \ |  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Caps | Left | Down | Rigth|      | BRK  |-------.    ,-------| Del  | End  | PgDn |      | ; :  | ' "  |
 * |------+------+------+------+------+------|  MUTE  |   |MICMUTE|------+------+------+------+------+------|
 * |LShift|  F9  | F10  | F11  | F12  | Menu |--------|   |-------|      |      | , >  | . >  | / ?  |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *            | LCTR | LGUI | LAlt |RAISE | /Space  /       \Enter \  |RAISE | RAlt | RGUI | RCTR |
 *            |      |      |      |      |/       /         \      \ |      |      |      |      |
 *            `----------------------------------'           '------''---------------------------'
 */

[_RAISE] = LAYOUT(
  KC_ESC,   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,                     KC_F6,   KC_F7,   KC_F8,   KC_MINUS, KC_EQUAL, KC_DEL,
  _______,  KC_ESC,   KC_UP, XXXXXXX, XXXXXXX, KC_PSCR,                     KC_INS,  KC_HOME, KC_PGUP, KC_LBRC, KC_RBRC, _______,
  _______, KC_LEFT, KC_DOWN, KC_RGHT, XXXXXXX, KC_BRK,                     KC_DEL,  KC_END,  KC_PGDN, XXXXXXX, _______, _______,
  _______, KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_APP, _______,   _______, XXXXXXX, XXXXXXX, _______, _______, _______, _______,
                    _______, _______, _______, _______, _______,   _______, _______, _______, _______, _______
)
};

#ifdef OLED_ENABLE
// https://joric.github.io/qle/
static void render_logo(void) {
    static const char PROGMEM raw_logo[] = {
        0,  0,  0,  0,204,204,204,204,252,252,240,240,192,192,  0,  0,192,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,  0,128,128,128,  0,  0,  0,128,128,128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,192,128,  0,  0,  0,  0,  0,  0,
        0,  0,207,207,  0,  0,255,255,255,255,252,252,255,255,  3,  3,  0,  0,  0,  0,  0,  0, 12, 12, 48, 48,252,252,255,255, 63, 63,243,243,195,195, 48, 48,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 63, 32, 32, 32, 31,  0,  0, 16, 32, 32, 31,  0,  0,  0, 31, 32, 32, 32, 31,  0,  0, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,204,109, 63, 30, 12,  0,  0,  0,
        0,  0,  3,  3,204,204,207,207,255,255, 60, 60, 15, 15,  3,  3, 12, 12,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255, 63, 63,255,255,192,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 48,120,252,182, 51, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,  0,  0,252, 32, 80,136,  4,  0,  0,252, 36, 36, 36,  4,  0,  0, 12, 16,224, 16, 12,  0,  0,252, 36, 36, 36,216,  0,  0,248,  4,  4,  4,248,  0,  0,240, 72, 68, 72,240,  0,  0,252, 36,100,164, 24,  0,  0,252,  4,  4,  4,248,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 12, 12,  3,  3, 15, 15, 63, 63, 63, 63, 51, 51, 48, 48,  3,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  1,  1,  1,  1,  1,  0,  0,  0,  0,  1,  0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0,  1,  1,  1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  1,  0,  0,  0,  1,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0,  0,
    };
    oled_write_raw_P(raw_logo, sizeof(raw_logo));
}

static void print_status_narrow(void) {
    // Print current mode
    oled_write_P(PSTR("\n"), false);

    if (keymap_config.swap_lctl_lgui) {
        oled_write_ln_P(PSTR("MAC"), false);
    } else {
        oled_write_P(PSTR("Linux"), false);
    }

    oled_write_P(PSTR("Qwrty"), false);
    oled_write_P(PSTR("\n"), false);

    // Print current layer
    switch (get_highest_layer(layer_state)) {
        case _QWERTY:
            oled_write_ln_P(PSTR("Base"), false);
            break;
        case _RAISE:
            oled_write_P(PSTR("Raise"), true);
            break;
        default:
            oled_write_P(PSTR("Undef"), false);
    }
    oled_write_P(PSTR("\n\n"), false);
    led_t led_usb_state = host_keyboard_led_state();
    oled_write_ln_P(PSTR("CPSLK"), led_usb_state.caps_lock);

    oled_write_P(PSTR("\n"), false);
    oled_write_P(PSTR(" DJO "), true);
    oled_write_P(PSTR("REV07"), true);  // TODO should be at the top of the file
}

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    if (is_keyboard_master()) {
        return OLED_ROTATION_270;
    }
    return rotation;
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
        print_status_narrow();
    } else {
        render_logo();
    }
    return false;
}

#endif

#ifdef ENCODER_ENABLE

bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index == 0) {
        if (clockwise) {
            tap_code(KC_VOLU);
        } else {
            tap_code(KC_VOLD);
        }
    } else if (index == 1) {
        if (clockwise) {
            tap_code(KC_PGDN);
        } else {
            tap_code(KC_PGUP);
        }
    }
    return true;
}

#endif
